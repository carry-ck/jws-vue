import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: HomeView,
    redirect: '/sys-admin/index', // 重定向
    children: [
      {
        path: '/sys-admin/index',
        component: () => import('../views/sys-admin/SystemAdminIndex.vue')
      },
      {
        path: '/owner/OwnerList',
        component: () => import('../views/owner/OwnerList.vue')
      }

    ]
  },
  {
    path: '/login',
    component: () => import('../views/login/LoginView.vue')
  },
  {
    path: '/register',
    component: () => import('../views/login/register.vue')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
